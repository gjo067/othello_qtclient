#ifndef GUIAPPLICATION_H
#define GUIAPPLICATION_H


// local
#include "gamemodel.h"

// othello library
#include <engine.h>

// qt
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QSizeF>

// stl
#include <memory>
#include <chrono>

using Clock = std::chrono::high_resolution_clock;


class GuiApplication : public QGuiApplication {
    Q_OBJECT
public:
    GuiApplication(int& argc, char** argv);
    ~GuiApplication() override = default;

private:
    othello::OthelloGameEngine m_game_engine;
    GameModel m_model;
    QQmlApplicationEngine m_app;
    static void aiTurnTask(othello::OthelloGameEngine&, const std::function<void()>&);
    void writeResultToFile(const othello::BitBoard&, long long);
    void handleEndOfGame();
    bool lastRoundSkipped = false;

    Clock::time_point startTime;
    int maxNumberOfGames;
    bool m_saveGames = false;
    bool m_displayResult = true;
    int gameNr = 0;
    bool m_aiTurnReady = false;
    int m_playerOneType = 0;
    int m_playerTwoType = 0;
    int playerOneWins = 0;
    int playerTwoWins = 0;
    int ties = 0;
    long elapsedTime = 0;
    int m_playerOneDepth = 4;
    int m_playerTwoDepth = 4;

private slots:
    void endGameAndQuit();
    void endOfGameActions();
    void startNextTurn();
    void boardClicked(int);
    void initNewGameFromOptions(int, int, int, int, int, bool, bool);

signals:
    void enqueueNextTurn();
    void gameEnded();
    void displayFinalScores(int, int);
    void readyForNextAiTurn();
    void updateCurrentScores(int, int);
    void updateTotalWins(int, int, int, int);

};   // END class GuiApplication

#endif   // GUIAPPLICATION_H
