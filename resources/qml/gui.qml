import QtQuick 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.4

ApplicationWindow {
  id: root

  signal endGameAndQuit()
  signal boardClicked(int board_pos)
  signal initNewGameFromOptions(int player_one_type, int player_two_type, int player_one_depth, int player_two_depth,
                                int number_of_games, bool save_games, bool display_result)

  signal displayFinalScores(int player_one_score, int player_two_score)
  signal updateCurrentScores(int player_one_score, int player_two_score)
  signal updateTotalWins(int playerOneWins, int playerTwoWins, int ties, int avgTime)
  signal displayMainMenu()

  onDisplayFinalScores: {
    final_score_window.player_one_final_score = player_one_score
    final_score_window.player_two_final_score = player_two_score
    final_score_window.visible = true
  }

  onUpdateCurrentScores: {
      nav.player_one_current_score = player_one_score
      nav.player_two_current_score = player_two_score
  }

  onUpdateTotalWins: {
      nav.playerOneTotalWins = playerOneWins;
      nav.playerTwoTotalWins = playerTwoWins;
      nav.totalTies = ties;
      nav.avgGameTime = avgTime;
  }

  onDisplayMainMenu: {
      startMenu.visible = true
      board.visible = false
      nav.visible = false
  }

  visible: true

  title: qsTr("Othello")
  width: 800
  height: 600

  menuBar: MenuBar {
        Menu {
          title: "Game"
          MenuItem {
            text: "Open menu"
            onTriggered: displayMainMenu()
          }
          MenuItem {
            text: "Quit"
            onTriggered: endGameAndQuit()
          }
        }
    }

  Rectangle {
      id: startMenu
      visible: true

      color: "lightgrey"

      width: 800
      height: 600

      Text {
          text: "Othello"
          font.pointSize: 30
          y: 10
          x: 330
      }

      ComboBox {
          id: player_one_type
          width: 200
          x: 170
          y: 60
          model: ["Player", "RandomMonkeyAI", "MiniMaxAI"]
      }

      Text {
          width: 100
          x: 380
          y: 65
          text: "vs."
      }

      ComboBox {
          id: player_two_type
          width: 200
          x: 410
          y: 60
          model: ["Player", "RandomMonkeyAI", "MiniMaxAI"]
      }

      Text {
          text: "Player 1 options:"
          width: 100
          x: 170
          y: 100
      }

      Text {
          text: "Color:"
          width: 50
          x: 170
          y: 125
      }

      ComboBox {
          width: 150
          x: 220
          y: 120
          model: ["white", "black", "blue", "red", "orange"]
          currentIndex: 0

          onCurrentIndexChanged: {
              board.player_one_color = currentText
          }
      }

      Rectangle {
          visible: player_one_type.currentIndex === 2 ? true : false

          Text {
              text: "Player 1 depth: "
              width: 150
              x: 170
              y: 155
          }

          TextField {
              id: player_one_depth
              text: "4"
              width: 50
              x: 320
              y: 150
          }
      }

      Text {
          text: "Player 2 options:"
          width: 100
          x: 410
          y: 100
      }

      Text {
          text: "Color:"
          width: 50
          x: 410
          y: 125
      }

      ComboBox {
          width: 150
          x: 460
          y: 120
          model: ["white", "black", "blue", "red", "orange"]
          currentIndex: 1

          onCurrentIndexChanged: {
              board.player_two_color = currentText
          }
      }

      Rectangle {
          visible: player_two_type.currentIndex === 2 ? true : false

          Text {
              text: "Player 2 depth: "
              width: 150
              x: 410
              y: 155
          }

          TextField {
              id: player_two_depth
              text: "4"
              width: 50
              x: 560
              y: 150
          }
      }

      Text {
          text: "Number of games:"
          width: 100
          x: 325
          y: 455
      }

      TextField {
          id: number_of_games
          text: "1"
          width: 50
          x: 425
          y: 450
      }

      CheckBox {
          id: save_games
          text: "Save games to file"
          checked: false
          x: 350
          y: 480
      }

      CheckBox {
          id: display_result
          text: "Display final scores after game"
          checked: true
          x: 350
          y: 510
      }

      Button {
          text: "Start"
          width: 60
          x: 370
          y: 550

          onClicked: {
              startMenu.visible = false
              board.visible = true
              nav.visible = true
              initNewGameFromOptions(player_one_type.currentIndex, player_two_type.currentIndex, player_one_depth.text, player_two_depth.text,
                                     number_of_games.text, save_games.checked, display_result.checked)
          }
      }
  }


  GridLayout {
    id: board
    visible: false

    property string player_one_color: "white"
    property string player_two_color: "black"

    width: 600
    height: 600
    columns: 8
    rows: 8
    columnSpacing: 0.2
    rowSpacing: 0.2

    Repeater {
      model: VisualDataModel {
        model: gamemodel
        delegate: Rectangle {

            id: board_square_rec

            property bool contains_mouse: false

            Layout.fillHeight: true
            Layout.fillWidth: true
            color: index % 2 === 0 ? (((index - index % 8) / 8) % 2 === 0 ? "#00aa00" : "green") : (((index - index % 8) / 8) % 2 === 0 ? "green" : "#00aa00")

            Rectangle {
                anchors.centerIn: parent
                width: Math.min(parent.width, parent.height) * 0.7
                height: width
                radius: width * 0.5

                color: {
                    if(occupied)
                        return playernr === 0 ? board.player_one_color : board.player_two_color
                    else if(contains_mouse && legalmove)
                        return gamemodel.currentPlayer === 0 ? board.player_one_color : board.player_two_color
                    else return board_square_rec.color
                }
            }

            border.color: "black"
            border.width: 1
            MouseArea {
              anchors.fill: parent
              onClicked: {
                boardClicked(piecenr)
              }

              hoverEnabled: true
              onContainsMouseChanged: board_square_rec.contains_mouse = containsMouse

            }
        }
      }
    }
  }

  Rectangle {
      id: nav
      visible: false

      property int player_one_current_score: 2
      property int player_two_current_score: 2
      property int playerOneTotalWins: 0
      property int playerTwoTotalWins: 0
      property int totalTies: 0
      property int avgGameTime: 0

      width: 200
      height: 600
      x: 600

      color: "grey"

      ColumnLayout {
          anchors.fill: parent
          anchors.margins: 20

          Text { text: "Current Score:" }
          Text { text: "Player One: " + nav.player_one_current_score }
          Text { text: "Player Two: " + nav.player_two_current_score }

          Item { height: 20 }

          Text { text: "Total wins:" }
          Text { text: "Player One: " + nav.playerOneTotalWins }
          Text { text: "Player Two: " + nav.playerTwoTotalWins }
          Text { text: "Ties: " + nav.totalTies }
          Text { text: "Avg game time: " + nav.avgGameTime  + "ms" }

          Item { height: 80 }
      }
  }

  Rectangle {
    id: final_score_window
    visible: false

    property int player_one_final_score: 0
    property int player_two_final_score: 0

    anchors.fill: parent
    anchors.margins: 50

    color: "red"
    opacity: 0.8

    ColumnLayout {
      anchors.fill: parent
      anchors.margins: 20

      Text{ Layout.preferredWidth: parent.width; height: 20;
            text: "Game Over!" }
      Item{ height: 20}
      Text{ Layout.preferredWidth: parent.width; height: 20;
            text: "Player one: " + final_score_window.player_one_final_score}
      Text{ Layout.preferredWidth: parent.width; height: 20;
            text: "Player two: " + final_score_window.player_two_final_score}
      Item{ Layout.fillHeight: true}
      Button{ text: "Ok"; onClicked: final_score_window.visible = false }
    }

  }
}
