#include "guiapplication.h"

// othello game library
#include <orangemonkey_ai.h>

// qt
#include <QQmlContext>
#include <QQuickItem>
#include <QQuickWindow>
#include <QTimer>
#include <QDebug>
#include <QThread>

// stl
#include <chrono>
#include <thread>

#include <fstream>
using namespace std::chrono_literals;


GuiApplication::GuiApplication(int& argc, char** argv)
    : QGuiApplication(argc, argv),
      m_game_engine{}, m_model{m_game_engine}, m_app{}
{

    m_app.rootContext()->setContextProperty("gamemodel", &m_model);

    m_app.load(QUrl("qrc:/qml/gui.qml"));

    auto* root_window = qobject_cast<QQuickWindow*>(m_app.rootObjects().first());
    if (root_window) {

        connect(root_window, SIGNAL(endGameAndQuit()), this,
                SLOT(endGameAndQuit()));

        connect(root_window, SIGNAL(initNewGameFromOptions(int, int, int, int, int, bool, bool)), this,
                SLOT(initNewGameFromOptions(int, int, int, int, int, bool, bool)));

        connect(root_window, SIGNAL(boardClicked(int)), this,
                SLOT(boardClicked(int)));

        connect(this, &GuiApplication::enqueueNextTurn, this,
                &GuiApplication::startNextTurn);

        connect(this, &GuiApplication::gameEnded, this,
                &GuiApplication::endOfGameActions);

        connect(this, SIGNAL(displayFinalScores(int, int)), root_window,
                SIGNAL(displayFinalScores(int, int)));

        connect(this, SIGNAL(updateCurrentScores(int, int)), root_window,
                SIGNAL(updateCurrentScores(int, int)));

        connect(this, SIGNAL(updateTotalWins(int, int, int, int)), root_window,
                SIGNAL(updateTotalWins(int, int, int, int)));
    }
}

void GuiApplication::startNextTurn()
{
    emit updateCurrentScores(static_cast<int>(m_game_engine.board()[0].count()), static_cast<int>(m_game_engine.board()[1].count()));
    m_model.update();

    m_game_engine.swapCurrentPlayer();

    // Check if the board is full
    if(othello::utility::occupiedPositions(m_game_engine.board()).all()) {
        handleEndOfGame();
        return;
    }

    // Check if no legal moves, if no skip turn, if more than one turn has been skipped end the game
    if(othello::utility::legalMoves(m_game_engine.board(), m_game_engine.currentPlayerId()).empty()) {
        if(lastRoundSkipped) {
            handleEndOfGame();
            return;
        }
        lastRoundSkipped = true;
        emit(enqueueNextTurn());
        return;
    }
    else {
        lastRoundSkipped = false;

        if(m_game_engine.currentPlayerType() == othello::PlayerType::AI) {
            std::thread{&GuiApplication::aiTurnTask, std::ref(m_game_engine), [&]{ emit enqueueNextTurn(); }}.detach();
        }
    }
}

void GuiApplication::writeResultToFile(const othello::BitBoard& board, long long time) {
    std::ofstream scores{"../../scores.csv", std::ios_base::app};
    scores << board[0].count() << "," << board[1].count() << "," << board[0] << "," << board[1] << ","
                               << (board[0].count() > board[1].count() ? "1" : "2") << "," << time << "\n";
}

void GuiApplication::handleEndOfGame()
{
    othello::BitBoard board = m_game_engine.board();
    auto time = std::chrono::duration_cast<std::chrono::milliseconds>(Clock::now() - startTime).count();

    elapsedTime += time;
    ++gameNr;

    if(board[0].count() == board[1].count()) ties++;
    else if(board[0].count() > board[1].count()) playerOneWins++;
    else playerTwoWins++;

    int averageGameTime = elapsedTime / gameNr;

    emit updateTotalWins(playerOneWins, playerTwoWins, ties, averageGameTime);

    if(m_displayResult)
        emit displayFinalScores(int(m_game_engine.board()[0].count()), int(m_game_engine.board()[1].count()));
    if(m_saveGames)
        writeResultToFile(board, time);

    if(gameNr < maxNumberOfGames) {
        m_game_engine.initNewGameFromOptions(m_playerOneType, m_playerTwoType, m_playerOneDepth, m_playerTwoDepth);
        m_model.update();
        startTime = Clock::now();
        emit enqueueNextTurn();
    }
    else {
        elapsedTime = 0;
        gameNr = 0;
        ties = 0;
        playerOneWins = 0;
        playerTwoWins = 0;
    }
}

void GuiApplication::aiTurnTask(othello::OthelloGameEngine& engine, const std::function<void()>& f) {
    engine.think(std::chrono::seconds{3s});
    f();
}

void GuiApplication::endGameAndQuit() { QGuiApplication::quit(); }


void GuiApplication::endOfGameActions()
{
}

void GuiApplication::boardClicked(int board_pos) {
    if(m_game_engine.currentPlayerType() == othello::PlayerType::Human
            && m_game_engine.performMoveForCurrentHuman(othello::BitPos(board_pos)))
        emit enqueueNextTurn();
}

void GuiApplication::initNewGameFromOptions(int playerOneType, int playerTwoType, int playerOneDepth, int playerTwoDepth,
                                            int numberOfGames, bool saveGames, bool displayResult)
{
    maxNumberOfGames = numberOfGames;
    m_saveGames = saveGames;
    m_displayResult = displayResult;
    m_playerOneType = playerOneType;
    m_playerTwoType = playerTwoType;
    m_playerOneDepth = playerOneDepth;
    m_playerTwoDepth = playerTwoDepth;
    m_game_engine.initNewGameFromOptions(playerOneType, playerTwoType, playerOneDepth, playerTwoDepth);
    m_model.update();
    startTime = Clock::now();
    emit enqueueNextTurn();
}
