
#include "guiapplication.h"

// engine
#include <engine.h>
#include <orangemonkey_ai.h>

// stl
#include <exception>
#include <string>
#include <iostream>

int main(int argc, char** argv) try {

    // Start application
    GuiApplication app(argc, argv);

    return app.exec();
}
catch (std::string exception) {
    std::cout << "Runtime exception: " << exception << std::endl;
}
catch (std::runtime_error exception) {
    std::cout << "Runtime exception: " << exception.what() << std::endl;
}
catch(std::exception& e) {
    std::cout << "Error: " << e.what() << std::endl;
}
catch(...) {
    std::cout << "Unknown Error!" << std::endl;
}
